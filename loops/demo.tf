resource "aws_instance" "sample" {
  ami                     = "ami-0667149a69bc2c367"
  instance_type           = "t2.micro"
  key_name                = "b47"
  count = var.proj1 ? 1 : 0

  tags = {
    Name = "Test-Machine-Remote-State-${count.index+1}"
  }
}

variable "proj1"{
  default = false 
}