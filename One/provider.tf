
provider "aws" {
    region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket          = "batch47-remotestate-dynamodb12"
    key             = "dev/ec2/terraform.tfstate"
    region          = "us-east-1"
    dynamodb_table  = "terraform-locking"
  }
}

provisioner "file" {
    source      = "/home/centos/.ssh"
    destination = "/home/centos/.ssh"
  }