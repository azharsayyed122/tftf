resource "aws_instance" "sample" {
  ami                     = "ami-0667149a69bc2c367"
  instance_type           = "t2.micro"
  vpc_security_group_ids  = [aws_security_group.allow_ssh.id]
  key_name                = "b47"

  tags = {
    Name = "Test-Machine-Remote-State"
  }

# Copies the private key file as the centos user using SSH
provisioner "file" {
  source      = "/home/centos/.ssh/id_rsa"
  destination = "/home/centos/.ssh/id_rsa"

  connection {
    type        = "ssh"
    user        = "centos"
    private_key = "${file("/home/centos/.ssh/id_rsa")}"
    host        = aws_instance.sample.private_ip
     }
   }
}